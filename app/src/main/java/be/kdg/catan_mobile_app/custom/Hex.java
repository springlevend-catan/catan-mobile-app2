package be.kdg.catan_mobile_app.custom;

public class Hex {
    private int imageReference;
    private int imageValue;

    public Hex(int imageReference, int imageValue) {
        this.imageReference = imageReference;
        this.imageValue = imageValue;
    }

    public int getImageReference() {
        return imageReference;
    }

    public int getImageValue() {
        return imageValue;
    }

    @Override
    public String toString() {
        return "Hex{" +
                "imageReference=" + imageReference +
                ", imageValue=" + imageValue +
                '}';
    }
}
