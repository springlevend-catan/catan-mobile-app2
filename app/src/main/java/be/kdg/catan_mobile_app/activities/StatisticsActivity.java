package be.kdg.catan_mobile_app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import be.kdg.catan_mobile_app.R;

public class StatisticsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        initialiseViews();
        addEventHandlers();
    }

    private void initialiseViews(){

    }

    private void addEventHandlers(){

    }
}
