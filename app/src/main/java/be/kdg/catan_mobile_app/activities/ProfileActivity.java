package be.kdg.catan_mobile_app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import be.kdg.catan_mobile_app.R;

public class ProfileActivity extends AppCompatActivity {
    Button btnChangePassword;
    TextView tvOldPassword;
    TextView tvNewPassword;
    TextView tvRepeatPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initialiseViews();
        addEventHandlers();
    }

    private void initialiseViews(){
        btnChangePassword = findViewById(R.id.btn_change_password);
        tvOldPassword = findViewById(R.id.tv_old_password);
        tvNewPassword = findViewById(R.id.tv_new_password);
        tvRepeatPassword = findViewById(R.id.tv_repeat_password);
    }

    private void addEventHandlers(){
        makeTextViewsVisible();
    }

    private void makeTextViewsVisible(){
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOldPassword.setVisibility(View.VISIBLE);
                tvNewPassword.setVisibility(View.VISIBLE);
                tvRepeatPassword.setVisibility(View.VISIBLE);
            }
        });
    }
}
