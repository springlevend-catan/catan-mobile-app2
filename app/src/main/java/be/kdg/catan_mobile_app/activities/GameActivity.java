package be.kdg.catan_mobile_app.activities;

import android.app.Dialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.adapter.PlayerGameAdapter;
import be.kdg.catan_mobile_app.adapter.RowAdapter;
import be.kdg.catan_mobile_app.model.ActionDTO;
import be.kdg.catan_mobile_app.model.CardType;
import be.kdg.catan_mobile_app.model.Game;
import be.kdg.catan_mobile_app.model.TileState;
import be.kdg.catan_mobile_app.model.TileType;
import be.kdg.catan_mobile_app.service.APIClient;
import be.kdg.catan_mobile_app.service.GameLogicService;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import pl.polidea.view.ZoomView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.naiksoftware.stomp.StompClient;

public class GameActivity extends AppCompatActivity {
    private String username;
    private TextView tvUsername;
    private FrameLayout llGameboard;
    private ZoomView zoomView;
    private Dialog myDialog;
    private TextView tvClose;
    private ImageView ivHammer;
    private ImageView ivCards;
    private ImageView ivScale;
    private int valueDice = 0;
    ImageView dice1;
    ImageView dice2;
    LinearLayout devs;
    LinearLayout res;
    ImageView iv_victory;
    ImageView iv_streetbuilding;
    ImageView iv_monopoly;
    ImageView iv_knight;
    ImageView iv_invention;

    TextView dev_knight;
    TextView dev_victory;
    TextView dev_streetBuilding;
    TextView dev_monopoly;
    TextView dev_invention;
    TextView requiredAction;
    private String TAG = "GAME";

    static String gameUrl;
    static String token;

    StompClient gameClient;
    boolean movingRobber = false;
    boolean isBuilding = false;
    boolean viewDev = false;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    Game game;
    ImageView btn_dice;
    List<List<TileState>> oldTiles;
    private boolean isBuildingCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        gameUrl = this.getIntent().getStringExtra("gameUrl");
        username = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("username", "noUsername");

        if (gameUrl == null) {
            String gameJSON = "{\"editText\":\"-521175720\",\"tiles\":[[{\"x\":0,\"y\":0,\"tileType\":\"PORT_WOOL\",\"number\":0,\"buildings\":[]},{\"x\":0,\"y\":1,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":0,\"y\":2,\"tileType\":\"PORT_WOOD\",\"number\":0,\"buildings\":[]},{\"x\":0,\"y\":3,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":1,\"y\":0,\"tileType\":\"PORT_ORE\",\"number\":0,\"buildings\":[]},{\"x\":1,\"y\":1,\"tileType\":\"ORE\",\"number\":4,\"buildings\":[{\"buildingType\":\"STREET\",\"index\":0,\"playerOrder\":0},{\"buildingType\":\"STREET\",\"index\":1,\"playerOrder\":1},{\"buildingType\":\"STREET\",\"index\":2,\"playerOrder\":1},{\"buildingType\":\"STREET\",\"index\":5,\"playerOrder\":1}]},{\"x\":1,\"y\":2,\"tileType\":\"BRICK\",\"number\":6,\"buildings\":[]},{\"x\":1,\"y\":3,\"tileType\":\"GRAIN\",\"number\":9,\"buildings\":[]},{\"x\":1,\"y\":4,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":2,\"y\":0,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":2,\"y\":1,\"tileType\":\"WOOL\",\"number\":2,\"buildings\":[]},{\"x\":2,\"y\":2,\"tileType\":\"WOOD\",\"number\":5,\"buildings\":[]},{\"x\":2,\"y\":3,\"tileType\":\"BRICK\",\"number\":12,\"buildings\":[{\"buildingType\":\"VILLAGE\",\"index\":4,\"playerOrder\":0}]},{\"x\":2,\"y\":4,\"tileType\":\"WOOD\",\"number\":4,\"buildings\":[{\"buildingType\":\"VILLAGE\",\"index\":3,\"playerOrder\":1}]},{\"x\":2,\"y\":5,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]}],[{\"x\":3,\"y\":0,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]},{\"x\":3,\"y\":1,\"tileType\":\"ORE\",\"number\":9,\"buildings\":[{\"buildingType\":\"STREET\",\"index\":4,\"playerOrder\":1}]},{\"x\":3,\"y\":2,\"tileType\":\"GRAIN\",\"number\":8,\"buildings\":[]},{\"x\":3,\"y\":3,\"tileType\":\"WOOD\",\"number\":8,\"buildings\":[]},{\"x\":3,\"y\":4,\"tileType\":\"WOOL\",\"number\":10,\"buildings\":[{\"buildingType\":\"VILLAGE\",\"index\":4,\"playerOrder\":0}]},{\"x\":3,\"y\":5,\"tileType\":\"WOOD\",\"number\":3,\"buildings\":[]},{\"x\":3,\"y\":6,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":4,\"y\":0,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":4,\"y\":1,\"tileType\":\"GRAIN\",\"number\":5,\"buildings\":[]},{\"x\":4,\"y\":2,\"tileType\":\"GRAIN\",\"number\":10,\"buildings\":[]},{\"x\":4,\"y\":3,\"tileType\":\"BRICK\",\"number\":11,\"buildings\":[]},{\"x\":4,\"y\":4,\"tileType\":\"ORE\",\"number\":3,\"buildings\":[{\"buildingType\":\"CITY\",\"index\":3,\"playerOrder\":0}]},{\"x\":4,\"y\":5,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]}],[{\"x\":5,\"y\":0,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]},{\"x\":5,\"y\":1,\"tileType\":\"WOOL\",\"number\":6,\"buildings\":[{\"buildingType\":\"STREET\",\"index\":3,\"playerOrder\":1}]},{\"x\":5,\"y\":2,\"tileType\":\"WOOL\",\"number\":11,\"buildings\":[]},{\"x\":5,\"y\":3,\"tileType\":\"DESERT\",\"number\":0,\"buildings\":[]},{\"x\":5,\"y\":4,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":6,\"y\":0,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":6,\"y\":1,\"tileType\":\"PORT_GRAIN\",\"number\":0,\"buildings\":[]},{\"x\":6,\"y\":2,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":6,\"y\":3,\"tileType\":\"PORT_BRICK\",\"number\":0,\"buildings\":[]}]],\"players\":[{\"username\":\"Player1\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":0,\"colorId\":0,\"resources\":{\"GRAIN\":3,\"WOOD\":0,\"ORE\":5,\"BRICK\":0,\"WOOL\":1},\"active\":true,\"host\":true},{\"username\":\"Player2\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":1,\"colorId\":1,\"resources\":{\"GRAIN\":3,\"WOOD\":1,\"ORE\":0,\"BRICK\":6,\"WOOL\":0},\"active\":true,\"host\":false},{\"username\":\"Player3\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":2,\"colorId\":1,\"resources\":{\"GRAIN\":3,\"WOOD\":1,\"ORE\":0,\"BRICK\":6,\"WOOL\":0},\"active\":true,\"host\":false},{\"username\":\"Player4\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":3,\"colorId\":1,\"resources\":{\"GRAIN\":3,\"WOOD\":1,\"ORE\":0,\"BRICK\":6,\"WOOL\":0},\"active\":true,\"host\":false}],\"requiredAction\":\"\",\"lastAction\":null,\"playerWithTurn\":{\"username\":\"Player3\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":0,\"colorId\":0,\"resources\":{\"GRAIN\":3,\"WOOD\":0,\"ORE\":5,\"BRICK\":0,\"WOOL\":1},\"active\":true,\"host\":true},\"robber\":[3,4]}";
            this.username = "Player1";
            try {
                updateViews(gameJSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            connectToGameSocket();
        }
        initialiseViews();
        addEventHandlers();
    }

    private void initialiseViews() {
        username = getIntent().getStringExtra("USERNAME");
        ivHammer = findViewById(R.id.iv_hammer);
        ivScale = findViewById(R.id.iv_scale);
        ivCards = findViewById(R.id.iv_cards);
        dice1 = findViewById(R.id.dice1);
        dice2 = findViewById(R.id.dice2);
        btn_dice = findViewById(R.id.btn_dice);
        devs = findViewById(R.id.dev);
        devs.setVisibility(View.INVISIBLE);
        res = findViewById(R.id.res);
        iv_victory = findViewById(R.id.iv_victory);
        iv_victory.setImageResource(R.drawable.dev_vict);
        iv_streetbuilding = findViewById(R.id.iv_streetbuilding);
        iv_streetbuilding.setImageResource(R.drawable.dev_street_building);
        iv_monopoly = findViewById(R.id.iv_monopoly);
        iv_monopoly.setImageResource(R.drawable.dev_monopoly);
        iv_knight = findViewById(R.id.iv_knight);
        iv_knight.setImageResource(R.drawable.dev_knight);
        iv_invention = findViewById(R.id.iv_invention);
        iv_invention.setImageResource(R.drawable.dev_invention);
        dev_knight = findViewById(R.id.tv_knight);
        dev_victory = findViewById(R.id.tv_victory);
        dev_streetBuilding = findViewById(R.id.tv_streetbuilding);
        dev_monopoly = findViewById(R.id.tv_monopoly);
        dev_invention = findViewById(R.id.tv_invention);
        requiredAction = findViewById(R.id.requiredAction);
    }

    private void addEventHandlers() {

        iv_invention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Work in progress, use invention on the web versrion", Toast.LENGTH_LONG).show();
            }
        });

        iv_knight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectMapper objectMapper = new ObjectMapper();
                ActionDTO actionDTO = new ActionDTO(token, "", "PLAY_KNIGHT_CARD");
                try {
                    gameClient.send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });

        iv_monopoly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Work in progress, use monopoly on the web versrion", Toast.LENGTH_LONG).show();

            }
        });

        iv_streetbuilding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectMapper objectMapper = new ObjectMapper();
                ActionDTO actionDTO = new ActionDTO(token, "", "PLAY_STREET_BUILDING");
                try {
                    gameClient.send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });

        iv_victory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectMapper objectMapper = new ObjectMapper();
                ActionDTO actionDTO = new ActionDTO(token, "", "PLAY_VICTORY_CARD");
                try {
                    gameClient.send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });

        ivCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewDev) {
                    ivCards.setImageResource(R.drawable.btn_dev);
                } else {
                    ivCards.setImageResource(R.drawable.btn_res);
                }
                viewDev = !viewDev;
                setValues();
            }
        });

        ivHammer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isBuilding) {
                    popupBuild();
                } else {
                    isBuilding = false;
                    isBuildingCity = false;
                    game.setTiles(oldTiles);
                    setValues();
                }
            }
        });

        ivScale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupTrade();
            }
        });

        btn_dice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionDTO actionDTO;
                if (game.getRequiredAction() != null && game.getRequiredAction().equals("DICE")) {
                    actionDTO = new ActionDTO(token, "", "DICE");
                } else {
                    actionDTO = new ActionDTO(token, "", "END_TURN");
                }
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    gameClient.send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                    setValues();
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void connectToGameSocket() {

        gameClient = APIClient.getGameClient();
        token = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("token", "noToken");

        Disposable disposable = gameClient.topic("/topic/game/" + gameUrl).subscribe(msg -> updateViews(msg.getPayload()), error -> {
            Log.d(TAG, error.getMessage());
        });

        compositeDisposable.add(disposable);

        gameClient.send("/app/game/join/" + gameUrl, token).subscribe();
    }

    private void updateViews(String payload) throws IOException {
        runOnUiThread(() -> {
            try {
                Game game = new ObjectMapper().readValue(payload, Game.class);
                if (game != null) {
                    this.game = game;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            setRequiredAction();
            setValues();
            playComputer();

        });
    }

    private void setValues() {
        username = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("username", "noUsername");
        if (game.getRequiredAction() != null) {
            requiredAction.setText(game.getRequiredAction());
        } else {
            requiredAction.setText("No action required");
        }
        addGridHexToGameboard();
        addPlayers();
        setResources();
        setDice();
        setBuilding();
        setDevs();
    }

    private void playComputer() {
        if (game.getPlayerWithTurn().isComputer()) {
            gameClient.send("/app/game/playComputer/" + gameUrl, game.getPlayerWithTurn().getUsername()).subscribe();
        }
    }

    private void setDevs() {
        if (viewDev) {
            res.setVisibility(View.INVISIBLE);
            devs.setVisibility(View.VISIBLE);
        } else {
            res.setVisibility(View.VISIBLE);
            devs.setVisibility(View.INVISIBLE);
        }
        dev_invention.setText(String.valueOf(game.findPlayerByUsername(username).getDevelopmentCards().get(CardType.INVENTION)));
        dev_knight.setText(String.valueOf(game.findPlayerByUsername(username).getDevelopmentCards().get(CardType.KNIGHT)));
        dev_monopoly.setText(String.valueOf(game.findPlayerByUsername(username).getDevelopmentCards().get(CardType.YEAR_OF_PLENTY)));
        dev_streetBuilding.setText(String.valueOf(game.findPlayerByUsername(username).getDevelopmentCards().get(CardType.STREET_BUILDING)));
        dev_victory.setText(String.valueOf(game.findPlayerByUsername(username).getDevelopmentCards().get(CardType.VICTORY)));
    }

    private void setBuilding() {
        if (isBuilding) {
            ivHammer.setImageResource(R.drawable.btn_cancel);
        } else {
            ivHammer.setImageResource(R.drawable.build_hammer);
        }
    }

    private void setRequiredAction() {
        movingRobber = false;
        if (game.getRequiredAction() != null) {
            if (game.getRequiredAction().equals("BUILD_VILLAGE")) {
                setVillageBuildingSpots();
            } else if (game.getRequiredAction().equals("BUILD_STREET") || game.getRequiredAction().equals("BUILD_TWO_STREETS") || game.getRequiredAction().equals("BUILD_SECOND_STREET")) {
                setStreetBuildingSpots();
            } else if (game.getRequiredAction().equals("MOVE_ROBBER") && !movingRobber) {
                movingRobber = true;
                setValues();
            }
        }
    }

    private void setStreetBuildingSpots() {
        oldTiles = game.getTiles();
        GameLogicService gameLogicService = APIClient.getGameLogicClient(token).create(GameLogicService.class);
        Call<List<List<TileState>>> tilesCall = gameLogicService.getStreetSpots(gameUrl);

        tilesCall.enqueue(new Callback<List<List<TileState>>>() {
            @Override
            public void onResponse(Call<List<List<TileState>>> call, Response<List<List<TileState>>> response) {
                List<List<TileState>> tiles = new ArrayList<>();
                System.out.println("Street_spots" + response.body());
                tiles = response.body();
                game.setTiles(tiles);
                runOnUiThread(() -> setValues());
            }

            @Override
            public void onFailure(Call<List<List<TileState>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setVillageBuildingSpots() {
        oldTiles = game.getTiles();
        GameLogicService gameLogicService = APIClient.getGameLogicClient(token).create(GameLogicService.class);
        Call<List<List<TileState>>> tilesCall = gameLogicService.getVillageSpots(gameUrl);

        tilesCall.enqueue(new Callback<List<List<TileState>>>() {
            @Override
            public void onResponse(Call<List<List<TileState>>> call, Response<List<List<TileState>>> response) {
                List<List<TileState>> tiles = new ArrayList<>();
                System.out.println("Village_spots" + response.body());
                tiles = response.body();
                game.setTiles(tiles);
                runOnUiThread(() -> setValues());
            }

            @Override
            public void onFailure(Call<List<List<TileState>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setDice() {

        if (game.getDice() != null) {
            dice1.setImageResource(getDiceResource(game.getDice()[0]));
            dice2.setImageResource(getDiceResource(game.getDice()[1]));
        }

        if (game.getRequiredAction() != null && game.getRequiredAction().equals("DICE")) {
            btn_dice.setImageResource(R.drawable.btn_dice_wood);
        } else {
            btn_dice.setImageResource(R.drawable.btn_end_turn);
        }
    }

    private int getDiceResource(int die) {
        switch (die) {
            case 1:
                return R.drawable.dice_1;
            case 2:
                return R.drawable.dice_2;
            case 3:
                return R.drawable.dice_3;
            case 4:
                return R.drawable.dice_4;
            case 5:
                return R.drawable.dice_5;
            case 6:
                return R.drawable.dice_6;
        }
        return R.drawable.dice_1;
    }

    private void addPlayers() {
        RecyclerView playerList = findViewById(R.id.playerList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        PlayerGameAdapter playerGameAdapter = new PlayerGameAdapter(game.getPlayers(), 0, game.getPlayerWithTurn().getUsername());
        playerList.setLayoutManager(layoutManager);
        playerList.setHasFixedSize(true);
        playerList.setAdapter(playerGameAdapter);
    }

    private void setResources() {
        TextView tv_grain = findViewById(R.id.tv_grain);
        TextView tv_wood = findViewById(R.id.tv_wood);
        TextView tv_wool = findViewById(R.id.tv_wool);
        TextView tv_brick = findViewById(R.id.tv_brick);
        TextView tv_ore = findViewById(R.id.tv_ore);

        Map<TileType, Integer> resources = this.game.getPlayers().stream().filter(p -> p.getUsername().equals(this.username)).findFirst().get().getResources();
        tv_grain.setText(String.valueOf(resources.get(TileType.GRAIN)));
        tv_wood.setText(String.valueOf(resources.get(TileType.WOOD)));
        tv_wool.setText(String.valueOf(resources.get(TileType.WOOL)));
        tv_brick.setText(String.valueOf(resources.get(TileType.BRICK)));
        tv_ore.setText(String.valueOf(resources.get(TileType.ORE)));
    }

    private void addGridHexToGameboard() {

        View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_board_rows, null, false);
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
        ListView rows = v.findViewById(R.id.rows);
        ListAdapter rowAdapter = new RowAdapter(rows.getContext(), (ArrayList<List<TileState>>) game.getTiles(), gameUrl, token, game.getRobber(), movingRobber, isBuildingCity, username);
        rows.setAdapter(rowAdapter);
        rows.setBackgroundResource(R.drawable.bg_paper_board);
        llGameboard = findViewById(R.id.ll_gameboard);
        llGameboard.removeAllViews();
        zoomView = new ZoomView(this);
        zoomView.addView(v);
        zoomView.setMaxZoom(4);
        llGameboard.addView(zoomView);
    }

    private void popupInvention() {
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.activity_invention);
        tvClose = myDialog.findViewById(R.id.txtclose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    private void popupTrade() {
        myDialog = new Dialog(this);

        View view = (View) LayoutInflater.from(this)
                .inflate(R.layout.activity_invention, null, false);

        myDialog.setContentView(view);
        tvClose = myDialog.findViewById(R.id.txtclose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    private void popupBuild() {
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.activity_build);

        tvClose = myDialog.findViewById(R.id.txtclose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        Button btn_build_village = myDialog.findViewById(R.id.btn_build_village);
        Button btn_build_city = myDialog.findViewById(R.id.btn_build_city);
        Button btn_build_street = myDialog.findViewById(R.id.btn_build_street);
        Button btn_build_dev = myDialog.findViewById(R.id.btn_build_dev);

        btn_build_village.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBuilding = true;
                setVillageBuildingSpots();
                myDialog.dismiss();
            }
        });

        btn_build_street.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBuilding = true;
                setStreetBuildingSpots();
                myDialog.dismiss();
            }
        });

        btn_build_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBuildingCity = true;
                isBuilding = true;
                setValues();
                myDialog.dismiss();
            }
        });

        btn_build_dev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectMapper objectMapper = new ObjectMapper();
                ActionDTO actionDTO = new ActionDTO(token, "", "BUY_DEVELOPMENT_CARD");
                try {
                    gameClient.send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });


        myDialog.show();
    }

}

