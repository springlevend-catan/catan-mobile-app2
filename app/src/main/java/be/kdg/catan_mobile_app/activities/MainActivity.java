package be.kdg.catan_mobile_app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.model.User;
import be.kdg.catan_mobile_app.service.APIClient;
import be.kdg.catan_mobile_app.service.AuthenticationService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView tvRegister;
    private Button btnLogin;
    private EditText etUsername;
    private EditText etPassword;
    private String username;
    private SharedPreferences prefs;
    private View view;
    private ImageView ivEnglish;
    private ImageView ivDutch;


    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocal();
        setContentView(R.layout.activity_main);
        intitialiseViews();
        addEventHandlers();
        prefs = MainActivity.this.getSharedPreferences("PREFS", Context.MODE_PRIVATE);
    }

    private void intitialiseViews() {

        tvRegister = findViewById(R.id.tv_no_account);
        btnLogin = findViewById(R.id.btn_login);
        etUsername = findViewById(R.id.input_username);
        etPassword = findViewById(R.id.et_password);
        view = findViewById(R.id.vw_background);
        ivEnglish = findViewById(R.id.iv_english);
        ivDutch = findViewById(R.id.iv_dutch);
    }


    private void addEventHandlers() {
        setLanguage();

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                username = etUsername.getText().toString();
                intent.putExtra("USERNAME", username);
                startActivity(intent);

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateFields()) {
                    AuthenticationService authenticationService = APIClient.getAuthenticationClient().create(AuthenticationService.class);
                    User user = new User(etUsername.getText().toString(), etPassword.getText().toString());
                    Call<String> token = authenticationService.signIn(user);

                    token.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            System.out.println("de response : " + response.body());
                            if (response.body() == null) {
                                Toast.makeText(getApplicationContext(), "Bad Credentials!!", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(getApplicationContext(), response.body(), Toast.LENGTH_LONG).show();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("token", response.body());

                            username = etUsername.getText().toString();
                            editor.putString("username", username);
                            editor.apply();
                            System.out.println("USERNAME");
                            System.out.println(prefs.getString("username", "token niet gevonden!"));

                            System.out.println("TOKEN");
                            System.out.println(prefs.getString("token", "token niet gevonden!"));

                            Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                            intent.putExtra("USERNAME", username);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

                }

            }
        });
    }


    private boolean validateFields() {
        if (etPassword.getText().length() < 8) {
            Toast.makeText(getApplicationContext(), "Your password has to be at least 8 characters", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etUsername.getText().length() <= 0) {
            Toast.makeText(getApplicationContext(), "Your username has to be filled in", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void setLanguage() {
        ivEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLocal("en");
                recreate();
            }
        });

        ivDutch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLocal("nl");
                recreate();
            }
        });
    }

    private void setLocal(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("my_lang", language);
        editor.apply();
    }

    public void loadLocal() {
        SharedPreferences prefs = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = prefs.getString("my_lang", "");
        setLocal(language);
    }

}
