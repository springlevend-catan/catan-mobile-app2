package be.kdg.catan_mobile_app.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.adapter.PlayerAdapter;
import be.kdg.catan_mobile_app.model.Lobby;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompMessage;

public class LobbyActivity extends AppCompatActivity {
    Button btnLeave;
    Button btnStartGame;
    Button btnAddComputer;

    private StompClient lobbyClient;
    private String TAG = "LOBBY";
    String token;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    ListView lv_players;
    Lobby lobby;
    String url;
    String testGame ="{\"editText\":\"-521175720\",\"tiles\":[[{\"x\":0,\"y\":0,\"tileType\":\"PORT_WOOL\",\"number\":0,\"buildings\":[]},{\"x\":0,\"y\":1,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":0,\"y\":2,\"tileType\":\"PORT_WOOD\",\"number\":0,\"buildings\":[]},{\"x\":0,\"y\":3,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":1,\"y\":0,\"tileType\":\"PORT_ORE\",\"number\":0,\"buildings\":[]},{\"x\":1,\"y\":1,\"tileType\":\"ORE\",\"number\":4,\"buildings\":[{\"buildingType\":\"STREET\",\"index\":0,\"playerOrder\":0},{\"buildingType\":\"STREET\",\"index\":1,\"playerOrder\":1},{\"buildingType\":\"STREET\",\"index\":2,\"playerOrder\":1},{\"buildingType\":\"STREET\",\"index\":5,\"playerOrder\":1}]},{\"x\":1,\"y\":2,\"tileType\":\"BRICK\",\"number\":6,\"buildings\":[]},{\"x\":1,\"y\":3,\"tileType\":\"GRAIN\",\"number\":9,\"buildings\":[]},{\"x\":1,\"y\":4,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":2,\"y\":0,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":2,\"y\":1,\"tileType\":\"WOOL\",\"number\":2,\"buildings\":[]},{\"x\":2,\"y\":2,\"tileType\":\"WOOD\",\"number\":5,\"buildings\":[]},{\"x\":2,\"y\":3,\"tileType\":\"BRICK\",\"number\":12,\"buildings\":[{\"buildingType\":\"VILLAGE\",\"index\":4,\"playerOrder\":0}]},{\"x\":2,\"y\":4,\"tileType\":\"WOOD\",\"number\":4,\"buildings\":[{\"buildingType\":\"VILLAGE\",\"index\":3,\"playerOrder\":1}]},{\"x\":2,\"y\":5,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]}],[{\"x\":3,\"y\":0,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]},{\"x\":3,\"y\":1,\"tileType\":\"ORE\",\"number\":9,\"buildings\":[{\"buildingType\":\"STREET\",\"index\":4,\"playerOrder\":1}]},{\"x\":3,\"y\":2,\"tileType\":\"GRAIN\",\"number\":8,\"buildings\":[]},{\"x\":3,\"y\":3,\"tileType\":\"WOOD\",\"number\":8,\"buildings\":[]},{\"x\":3,\"y\":4,\"tileType\":\"WOOL\",\"number\":10,\"buildings\":[{\"buildingType\":\"VILLAGE\",\"index\":4,\"playerOrder\":0}]},{\"x\":3,\"y\":5,\"tileType\":\"WOOD\",\"number\":3,\"buildings\":[]},{\"x\":3,\"y\":6,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":4,\"y\":0,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":4,\"y\":1,\"tileType\":\"GRAIN\",\"number\":5,\"buildings\":[]},{\"x\":4,\"y\":2,\"tileType\":\"GRAIN\",\"number\":10,\"buildings\":[]},{\"x\":4,\"y\":3,\"tileType\":\"BRICK\",\"number\":11,\"buildings\":[]},{\"x\":4,\"y\":4,\"tileType\":\"ORE\",\"number\":3,\"buildings\":[{\"buildingType\":\"CITY\",\"index\":3,\"playerOrder\":0}]},{\"x\":4,\"y\":5,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]}],[{\"x\":5,\"y\":0,\"tileType\":\"PORT\",\"number\":0,\"buildings\":[]},{\"x\":5,\"y\":1,\"tileType\":\"WOOL\",\"number\":6,\"buildings\":[{\"buildingType\":\"STREET\",\"index\":3,\"playerOrder\":1}]},{\"x\":5,\"y\":2,\"tileType\":\"WOOL\",\"number\":11,\"buildings\":[]},{\"x\":5,\"y\":3,\"tileType\":\"DESERT\",\"number\":0,\"buildings\":[]},{\"x\":5,\"y\":4,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]}],[{\"x\":6,\"y\":0,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":6,\"y\":1,\"tileType\":\"PORT_GRAIN\",\"number\":0,\"buildings\":[]},{\"x\":6,\"y\":2,\"tileType\":\"WATER\",\"number\":0,\"buildings\":[]},{\"x\":6,\"y\":3,\"tileType\":\"PORT_BRICK\",\"number\":0,\"buildings\":[]}]],\"players\":[{\"username\":\"Player1\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":0,\"colorId\":0,\"resources\":{\"GRAIN\":3,\"WOOD\":0,\"ORE\":5,\"BRICK\":0,\"WOOL\":1},\"active\":true,\"host\":true},{\"username\":\"Player2\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":1,\"colorId\":1,\"resources\":{\"GRAIN\":3,\"WOOD\":1,\"ORE\":0,\"BRICK\":6,\"WOOL\":0},\"active\":true,\"host\":false}, {\"username\":\"Player3\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":1,\"colorId\":1,\"resources\":{\"GRAIN\":3,\"WOOD\":1,\"ORE\":0,\"BRICK\":6,\"WOOL\":0},\"active\":true,\"host\":false}, {\"username\":\"Player4\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":1,\"colorId\":1,\"resources\":{\"GRAIN\":3,\"WOOD\":1,\"ORE\":0,\"BRICK\":6,\"WOOL\":0},\"active\":true,\"host\":false}],\"requiredAction\":\"\",\"lastAction\":null,\"playerWithTurn\":{\"username\":\"Player3\",\"points\":0,\"longestRoad\":0,\"numberOfKnights\":0,\"turnOrder\":0,\"colorId\":0,\"resources\":{\"GRAIN\":3,\"WOOD\":0,\"ORE\":5,\"BRICK\":0,\"WOOL\":1},\"active\":true,\"host\":true},\"robber\":[3,4]}";

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getIntent().getExtras().get("URL").toString();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            lobby = objectMapper.readValue("{\"host\":{\"userId\":5,\"username\":\"samT\",\"email\":\"test@sam\"},\"users\":[],\"editText\":\"4\",\"size\":4,\"available\":true,\"closed\":false}",Lobby.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        subsribeToSocket();
        setContentView(R.layout.activity_lobby);
        initialiseViews();
        addEventHandlers();
    }

    private void subsribeToSocket() {

        lobbyClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "http://192.168.1.5:5500/ws/websocket");
        lobbyClient.lifecycle().subscribe(lifecycleEvent -> {
            switch (lifecycleEvent.getType()) {

                case OPENED:
                    Log.d(TAG, "Stomp connection opened");
                    break;

                case ERROR:
                    Log.e(TAG, "Error", lifecycleEvent.getException());
                    break;

                case CLOSED:
                    Log.d(TAG, "Stomp connection closed");
                    break;
            }
        });
        lobbyClient.connect();
        token = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("token", "noToken");


        Disposable disposable = lobbyClient.topic("/topic/lobby/" + url).subscribe(msg -> updateViews(msg.getPayload()), error -> {
            Log.d(TAG, error.getMessage());
        });

        compositeDisposable.add(disposable);

        lobbyClient.send("/app/lobby/" + url, token).subscribe();

    }


    private void initialiseViews() {
        btnLeave = findViewById(R.id.btn_leave);
        btnStartGame = findViewById(R.id.btn_start);
        btnAddComputer = findViewById(R.id.btn_computer);

        lv_players = findViewById(R.id.lv_players);


    }
    private void updateList(){
        PlayerAdapter playerAdapter = new PlayerAdapter(lv_players.getContext(), lobby.getAllUsers());
        lv_players.setAdapter(playerAdapter);
    }

    private void updateViews(String msg) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        lobby = objectMapper.readValue(msg, Lobby.class);

        if (lobby.getGameUrl()!=null){
            Intent intent = new Intent(LobbyActivity.this, GameActivity.class);
            intent.putExtra("gameUrl",lobby.getGameUrl());
            startActivity(intent);
        }
        runOnUiThread(this::updateList);
    }

    private void addEventHandlers() {
        btnLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LobbyActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lobbyClient.send("/app/lobby/start/" + url, url).subscribe();
            }
        });

        btnAddComputer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lobbyClient.send("/app/lobby/joinComputer/" + url).subscribe();
            }
        });
    }
}
