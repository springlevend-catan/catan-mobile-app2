package be.kdg.catan_mobile_app.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.adapter.LobbyAdapter;
import be.kdg.catan_mobile_app.model.Lobby;
import be.kdg.catan_mobile_app.service.APIClient;
import be.kdg.catan_mobile_app.service.GameLogicService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllLobiesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_lobies);
        inititialiseViews();
        addEventHandlers();
    }

    ListView lv_lobbies;
    Button joinViaUrl;
    EditText editText;

    private void inititialiseViews() {
        lv_lobbies = findViewById(R.id.lv_messages);
        joinViaUrl = findViewById(R.id.joinLobbyViaUrl);
        editText = findViewById(R.id.urlEdit);
        String token = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("token", "noToken");

        String username = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("username", "noToken");

        GameLogicService gameLogicService = APIClient.getGameLogicClient(token).create(GameLogicService.class);
        Call<List<Lobby>> call = gameLogicService.getAvailableLobbies();

        call.enqueue(new Callback<List<Lobby>>() {
            @Override
            public void onResponse(Call<List<Lobby>> call, Response<List<Lobby>> response) {
                List<Lobby> lobbies = response.body();
                ListAdapter lobbyAdapter = new LobbyAdapter(lv_lobbies.getContext(), lobbies, username);
                lv_lobbies.setAdapter(lobbyAdapter);
            }

            @Override
            public void onFailure(Call<List<Lobby>> call, Throwable t) {

            }
        });

        joinViaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = editText.getText().toString();
                Intent intent = new Intent(AllLobiesActivity.this, LobbyActivity.class);
                intent.putExtra("USERNAME", username);
                intent.putExtra("URL", url);
                startActivity(intent);
            }
        });
    }

    private void addEventHandlers() {

    }
}
