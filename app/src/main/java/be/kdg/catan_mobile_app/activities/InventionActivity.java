package be.kdg.catan_mobile_app.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.adapter.ResourceIconAdapter;
import be.kdg.catan_mobile_app.model.ActionDTO;
import be.kdg.catan_mobile_app.model.InventionData;
import be.kdg.catan_mobile_app.model.TileType;
import be.kdg.catan_mobile_app.service.APIClient;

public class InventionActivity extends AppCompatActivity {

    List<TileType> resources;

    RecyclerView selectedRes;
    ImageView grain;
    ImageView ore;
    ImageView wood;
    ImageView wool;
    ImageView brick;
    Button submitInvention;
    String gameurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invention);
        resources = new ArrayList<>();
        initialiseViews();
        addEventHandlers();
    }

    private void addEventHandlers() {
        grain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addResource(TileType.GRAIN);
            }
        });


        submitInvention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectMapper objectMapper = new ObjectMapper();
                InventionData inventionData = new InventionData(resources);
                try {
                    ActionDTO actionDTO = new ActionDTO(GameActivity.token, objectMapper.writeValueAsString(inventionData), "PLAY_INVENTION_CARD");
                    APIClient.getGameClient().send("/app/game/" + GameActivity.gameUrl, objectMapper.writeValueAsString(actionDTO));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addResource(TileType resource) {
        if (resources.size() < 2) {
            resources.add(resource);
        }
        initialiseViews();
    }

    private void initialiseViews() {
        selectedRes = findViewById(R.id.selected_res);
        grain = findViewById(R.id.grain);
        ore = findViewById(R.id.ore);
        wood = findViewById(R.id.wood);
        wool = findViewById(R.id.wool);
        brick = findViewById(R.id.brick);
        submitInvention = findViewById(R.id.submit_invention);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(selectedRes.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        ResourceIconAdapter resourceIconAdapter = new ResourceIconAdapter(resources);

        selectedRes.setLayoutManager(linearLayoutManager);
        selectedRes.setAdapter(resourceIconAdapter);
    }


}
