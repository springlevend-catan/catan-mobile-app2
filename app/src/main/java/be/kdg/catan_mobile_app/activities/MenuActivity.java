package be.kdg.catan_mobile_app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.service.APIClient;
import be.kdg.catan_mobile_app.service.GameLogicService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {
    private ImageView ivProfile;
    private TextView tvWelcomeMessage;
    private Button btnCreateLobby;
    private Button btnStatistics;
    private Button btnMyGames;
    private Button btnAllLobies;
    private Button btnLogOut;
    private Button btnGamerules;
    private Button btnTest;
    private String username;
    private GameLogicService gameLogicService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String token = getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("token","no token");
        System.out.println("MENU: " + token);
        gameLogicService = APIClient.getGameLogicClient(token).create(GameLogicService.class);
        username = MenuActivity.this.getSharedPreferences("PREFS", Context.MODE_PRIVATE).getString("username", "username not found");

        loadLocal();
        setContentView(R.layout.activity_menu);
        initialiseViews();
        addEventHandlers();
    }

    private void initialiseViews() {
        ivProfile = findViewById(R.id.iv_profile);
        tvWelcomeMessage = findViewById(R.id.tv_welcome_message);
        btnCreateLobby = findViewById(R.id.btn_create_lobby);
        btnStatistics = findViewById(R.id.btn_statistics);
        btnMyGames = findViewById(R.id.btn_my_games);
        btnAllLobies = findViewById(R.id.btn_all_lobies);
        btnLogOut = findViewById(R.id.btn_log_out);
        btnGamerules = findViewById(R.id.btn_game_rules);
        btnTest = findViewById(R.id.btn_test);

        tvWelcomeMessage.setText(tvWelcomeMessage.getText() + " " + username + "!");
    }

    private void addEventHandlers() {

        // ivProfile.setOnClickListener(new View.OnClickListener() {
        //     @Override
        //     public void onClick(View view) {
        //         Intent intent = new Intent(MenuActivity.this, ProfileActivity.class);
        //         startActivity(intent);
        //     }
        // });
//
        btnCreateLobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<String> createLobbyCall = gameLogicService.createLobby("");

                createLobbyCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String url = response.body();
                        Intent intent = new Intent(MenuActivity.this, LobbyActivity.class);
                        intent.putExtra("USERNAME", username);
                        intent.putExtra("URL", url);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });


            }
        });

        btnStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, StatisticsActivity.class);
                startActivity(intent);
            }
        });

        btnMyGames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, MyGamesActivity.class);
                startActivity(intent);
            }
        });

        btnAllLobies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, AllLobiesActivity.class);
                startActivity(intent);
            }
        });

        btnGamerules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, GamerulesActivity.class);
                startActivity(intent);
            }
        });

        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, TestActivity.class);
                startActivity(intent);
            }
        });

        //TODO: logout functionality
//        btnLogOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
    }

    private void setLocal(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("my_lang", language);
        editor.apply();
    }

    private void loadLocal() {
        SharedPreferences prefs = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = prefs.getString("my_lang", "");
        setLocal(language);
    }
}
