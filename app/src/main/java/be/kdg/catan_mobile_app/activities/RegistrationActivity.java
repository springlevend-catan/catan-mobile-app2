package be.kdg.catan_mobile_app.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.model.User;
import be.kdg.catan_mobile_app.service.APIClient;
import be.kdg.catan_mobile_app.service.AuthenticationService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {
    TextView txtAlreadyMember;
    Button btnCreateAccount;
    AuthenticationService authenticationService;
    EditText et_firstname;
    EditText et_lastname;
    EditText et_username;
    EditText et_email;
    EditText et_password;
    EditText et_repeatpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initialiseViews();
        addEventHandlers();
        authenticationService = APIClient.getAuthenticationClient().create(AuthenticationService.class);
    }

    private void initialiseViews(){
        txtAlreadyMember = findViewById(R.id.tv_already_member);
        btnCreateAccount = findViewById(R.id.btn_create_account);
        et_firstname = findViewById(R.id.et_name);
        et_lastname= findViewById(R.id.et_lastname);
        et_username= findViewById(R.id.et_username);
        et_email= findViewById(R.id.et_email);
        et_password= findViewById(R.id.et_password);
        et_repeatpassword= findViewById(R.id.et_repeat_password);
    }

    private void addEventHandlers(){
        txtAlreadyMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_password.getText().toString().equals(et_repeatpassword.getText().toString())){
                    User user = new User(et_username.getText().toString(), et_password.getText().toString(),et_email.getText().toString(),et_firstname.getText().toString(),et_lastname.getText().toString());
                    Call<User> signupCall= authenticationService.signUp(user);

                    signupCall.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            Toast.makeText(RegistrationActivity.this,"Registration succesfull",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                            startActivity(intent);
                            System.out.println("response");
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Toast.makeText(RegistrationActivity.this,"Unexpected error, please try again later",Toast.LENGTH_SHORT).show();
                            System.out.println("error");

                        }
                    });

                }else  {
                    Toast.makeText(RegistrationActivity.this,"Passwords don't match",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
