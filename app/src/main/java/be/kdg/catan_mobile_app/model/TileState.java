package be.kdg.catan_mobile_app.model;

import java.util.*;

public class TileState {

    private int x;

    private int y;

    private TileType tileType;

    private int number;

    private List<Building> buildings;

    public TileState() {
    }

    @Override
    public String toString() {
        return "TileState{" +
                "x=" + x +
                ", y=" + y +
                ", tileType=" + tileType +
                ", number=" + number +
                '}';
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public TileType getTileType() {
        return tileType;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }
}
