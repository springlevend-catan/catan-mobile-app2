package be.kdg.catan_mobile_app.model;

public class ActionDTO {
    private String token;

    private String value;

    private String type;

    public ActionDTO(String token, String value, String type) {
        this.token = token;
        this.value = value;
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
