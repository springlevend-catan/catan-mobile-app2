package be.kdg.catan_mobile_app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;

public class Building {

    @JsonIgnore
    private TileState topLeftTile;

    private int index;

    private BuildingType buildingType;

    @JsonIgnore
    private List<TileState> neighbours;

    @JsonIgnore
    private Map<TileType, Integer> price;

    private String username;

    private int playerOrder;

    @JsonIgnore
    private boolean notAStreet;

    public void setNotAStreet(boolean notAStreet) {
        this.notAStreet = notAStreet;
    }

    public boolean isNotAStreet() {
        return buildingType != BuildingType.STREET;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, buildingType, username, playerOrder);
    }

    public TileState getTopLeftTile() {
        return topLeftTile;
    }

    public void setTopLeftTile(TileState topLeftTile) {
        this.topLeftTile = topLeftTile;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public BuildingType getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(BuildingType buildingType) {
        this.buildingType = buildingType;
    }

    public List<TileState> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<TileState> neighbours) {
        this.neighbours = neighbours;
    }

    public Map<TileType, Integer> getPrice() {
        return price;
    }

    public void setPrice(Map<TileType, Integer> price) {
        this.price = price;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPlayerOrder() {
        return playerOrder;
    }

    public void setPlayerOrder(int playerOrder) {
        this.playerOrder = playerOrder;
    }
}
