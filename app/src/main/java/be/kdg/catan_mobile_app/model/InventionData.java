package be.kdg.catan_mobile_app.model;

import java.util.List;

public class InventionData {
    private int[] resourcesToGet;

    public InventionData(List<TileType> resourcesToGet) {
        this.resourcesToGet = new int[]{0, 0, 0, 0, 0};

        for (TileType tileType : resourcesToGet) {
            switch (tileType) {
                case BRICK:
                    this.resourcesToGet[0]++;
                    break;
                case GRAIN:
                    this.resourcesToGet[0]++;
                    break;
                case ORE:
                    this.resourcesToGet[0]++;
                    break;
                case WOOD:
                    this.resourcesToGet[0]++;
                    break;
                case WOOL:
                    this.resourcesToGet[0]++;
                    break;
            }
        }
    }

    public int[] getResourcesToGet() {
        return resourcesToGet;
    }

    public void setResourcesToGet(int[] resourcesToGet) {
        this.resourcesToGet = resourcesToGet;
    }
}
