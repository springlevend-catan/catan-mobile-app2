package be.kdg.catan_mobile_app.model;

import java.util.List;

public class Lobby {

    public Lobby() {
    }

    private User host;
    private List<User> users;
    private String url;
    private String gameUrl;
    private int size;
    private boolean available;
    private boolean closed;

    public Lobby(User host, List<User> users, String url, int size, boolean available, boolean closed) {
        this.host = host;
        this.users = users;
        this.url = url;
        this.size = size;
        this.available = available;
        this.closed = closed;
    }

    public User getHost() {
        return host;
    }

    public void setHost(User host) {
        this.host = host;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public User[] getAllUsers() {
        User[] users = new User[getUsers().size() + 1];

        for (int i = 0; i < getUsers().size(); i++) {
            users[i] = new User(getUsers().get(i), false);
        }
        users[users.length - 1] = new User(host, true);
        return users;
    }

    public String getGameUrl() {
        return gameUrl;
    }

    public void setGameUrl(String gameUrl) {
        this.gameUrl = gameUrl;
    }
}
