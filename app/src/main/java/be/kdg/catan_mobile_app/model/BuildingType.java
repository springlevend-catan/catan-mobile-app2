package be.kdg.catan_mobile_app.model;

public enum BuildingType {
    VILLAGE, CITY, STREET, VILLAGE_BUILDING_SPOT, STREET_BUILDING_SPOT
}
