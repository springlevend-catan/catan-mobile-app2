package be.kdg.catan_mobile_app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private String url;

    private List<List<TileState>> tiles;

    private List<Player> players;

    private Player playerWithTurn;

    private int[] robber;

    private int[] dice;

    private int turnCounter;

    private String requiredAction="";

    private String lastAction;

    private int actionCounter;

    private boolean completed;

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getActionCounter() {
        return actionCounter;
    }

    public void setActionCounter(int actionCounter) {
        this.actionCounter = actionCounter;
    }

    public String getRequiredAction() {
        return requiredAction;
    }

    public void setRequiredAction(String requiredAction) {
        this.requiredAction = requiredAction;
    }

    public String getLastAction() {
        return lastAction;
    }

    public void setLastAction(String lastAction) {
        this.lastAction = lastAction;
    }

    public Game() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<List<TileState>> getTiles() {
        return tiles;
    }

    public List<TileState> allTiles(){
        List<TileState> allTiles = new ArrayList<>();
        for (List<TileState> tiles : this.getTiles()) {
            allTiles.addAll(tiles);
        }
        return allTiles;
    }

    public void setTiles(List<List<TileState>> tiles) {
        this.tiles = tiles;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Player getPlayerWithTurn() {
        return playerWithTurn;
    }

    public void setPlayerWithTurn(Player playerWithTurn) {
        this.playerWithTurn = playerWithTurn;
    }

    public int[] getRobber() {
        return robber;
    }

    public void setRobber(int[] robber) {
        this.robber = robber;
    }

    public int[] getDice() {
        return dice;
    }

    public void setDice(int[] dice) {
        this.dice = dice;
    }

    public int getTurnCounter() {
        return turnCounter;
    }

    public void setTurnCounter(int turnCounter) {
        this.turnCounter = turnCounter;
    }

    public Player findPlayerByUsername(String username){
        return players.stream().filter(p->p.getUsername().equals(username)).findFirst().get();
    }
}
