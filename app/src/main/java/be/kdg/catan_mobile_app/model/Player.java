package be.kdg.catan_mobile_app.model;

import java.util.HashMap;
import java.util.Map;

public class Player implements Comparable<Player> {

    private String username;

    private int points;

    private int longestRoad;

    private int numberOfKnights;

    private int turnOrder;

    private int colorId;

    private Map<TileType, Integer> resources;
    private boolean isActive;
    private boolean isHost;
    private boolean isComputer;

    public Player() {
    }

    @Override
    public int compareTo(Player o) {
        return Integer.compare(this.turnOrder, o.turnOrder);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getLongestRoad() {
        return longestRoad;
    }

    public void setLongestRoad(int longestRoad) {
        this.longestRoad = longestRoad;
    }

    public int getNumberOfKnights() {
        return numberOfKnights;
    }

    public void setNumberOfKnights(int numberOfKnights) {
        this.numberOfKnights = numberOfKnights;
    }

    public int getTurnOrder() {
        return turnOrder;
    }

    public void setTurnOrder(int turnOrder) {
        this.turnOrder = turnOrder;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    private Map<CardType, Integer> developmentCards;

    public Map<CardType, Integer> getDevelopmentCards() {
        return developmentCards;
    }

    public void setDevelopmentCards(Map<CardType, Integer> developmentCards) {
        this.developmentCards = developmentCards;
    }

    public Map<TileType, Integer> getResources() {
        return resources;
    }

    public void setResources(Map<TileType, Integer> resources) {
        this.resources = resources;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isHost() {
        return isHost;
    }

    public void setHost(boolean host) {
        isHost = host;
    }

    public boolean isComputer() {
        return isComputer;
    }

    public void setComputer(boolean computer) {
        isComputer = computer;
    }
}
