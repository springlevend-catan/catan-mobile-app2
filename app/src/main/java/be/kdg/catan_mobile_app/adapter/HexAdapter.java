package be.kdg.catan_mobile_app.adapter;

import android.content.Context;
import android.drm.DrmStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.custom.HexagonView;
import be.kdg.catan_mobile_app.model.ActionDTO;
import be.kdg.catan_mobile_app.model.BuildActionValue;
import be.kdg.catan_mobile_app.model.Building;
import be.kdg.catan_mobile_app.model.BuildingType;
import be.kdg.catan_mobile_app.model.MoveRobberData;
import be.kdg.catan_mobile_app.model.TileState;
import be.kdg.catan_mobile_app.service.APIClient;

public class HexAdapter extends RecyclerView.Adapter<HexAdapter.HexViewHolder> {

    private List<TileState> values;
    private Context context;
    private String gameUrl;
    private String token;
    private int[] robber;
    private boolean movingRobber;
    private boolean buildingCity;
    private String username;

    private int getBuildingResource(Building building) {
        int playerOrder = building.getPlayerOrder();
        switch (building.getBuildingType()) {
            case VILLAGE:
                switch (playerOrder) {
                    case 0:
                        return R.drawable.village_0;
                    case 1:
                        return R.drawable.village_1;
                    case 2:
                        return R.drawable.village_2;
                    case 3:
                        return R.drawable.village_3;
                }
            case CITY:
                switch (playerOrder) {
                    case 0:
                        return R.drawable.city_0;
                    case 1:
                        return R.drawable.city_1;
                    case 2:
                        return R.drawable.city_2;
                    case 3:
                        return R.drawable.city_3;
                }
            case STREET:
                switch (playerOrder) {
                    case 0:
                        return R.drawable.street0;
                    case 1:
                        return R.drawable.street1;
                    case 2:
                        return R.drawable.street2;
                    case 3:
                        return R.drawable.street3;
                }
            case VILLAGE_BUILDING_SPOT:
                return R.drawable.village_99;
            case STREET_BUILDING_SPOT:
                return R.drawable.street99;
        }
        return R.drawable.village_99;
    }

    public HexAdapter(List<TileState> values, Context context, String gameUrl, String token, int[] robber, boolean movingRobber, boolean buildingCity, String username) {
        this.values = values;
        this.context = context;
        this.gameUrl = gameUrl;
        this.token = token;
        this.robber = robber;
        this.movingRobber = movingRobber;
        this.buildingCity = buildingCity;
        this.username = username;
    }

    @Override
    public HexViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = (View) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.hex_view, viewGroup, false);
        return new HexViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HexViewHolder hexViewHolder, int i) {
        TileState tileState = values.get(i);

        setValues(tileState, hexViewHolder.hexView);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public static class HexViewHolder extends RecyclerView.ViewHolder {
        View hexView;

        public HexViewHolder(View hexView) {
            super(hexView);
            this.hexView = hexView;
        }
    }

    private void setValues(TileState tileState, View hexView) {

        HexagonView imageView = (HexagonView) hexView.findViewById(R.id.hex);

        int imageResource = R.drawable.tile_water;

        switch (tileState.getTileType()) {
            case WOOD:
                imageResource = R.drawable.tile_wood;
                break;
            case GRAIN:
                imageResource = R.drawable.tile_wheat;
                break;
            case WOOL:
                imageResource = R.drawable.tile_wool;
                break;
            case BRICK:
                imageResource = R.drawable.tile_stone;
                break;
            case ORE:
                imageResource = R.drawable.tile_ore;
                break;
            case DESERT:
                imageResource = R.drawable.tile_desert;
                break;
            case WATER:
                imageResource = R.drawable.tile_water;
                break;
            case PORT:
                imageResource = R.drawable.tile_water;
                break;
            case PORT_WOOD:
                imageResource = R.drawable.tile_water;
                break;
            case PORT_GRAIN:
                imageResource = R.drawable.tile_water;
                break;
            case PORT_WOOL:
                imageResource = R.drawable.tile_water;
                break;
            case PORT_BRICK:
                imageResource = R.drawable.tile_water;
                break;
            case PORT_ORE:
                imageResource = R.drawable.tile_water;
                break;
        }

        imageView.setImageResource(imageResource);

        TextView number = hexView.findViewById(R.id.number);
        number.setText(String.valueOf(tileState.getNumber()));

        ImageView village0 = hexView.findViewById(R.id.iv_village0);
        ImageView village1 = hexView.findViewById(R.id.iv_village1);
        ImageView village2 = hexView.findViewById(R.id.iv_village2);
        ImageView village3 = hexView.findViewById(R.id.iv_village3);
        ImageView village4 = hexView.findViewById(R.id.iv_village4);
        ImageView village5 = hexView.findViewById(R.id.iv_village5);

        List<ImageView> villages = new ArrayList<>();
        villages.add(village0);
        villages.add(village1);
        villages.add(village2);
        villages.add(village3);
        villages.add(village4);
        villages.add(village5);

        ImageView street0 = hexView.findViewById(R.id.street0);
        ImageView street1 = hexView.findViewById(R.id.street1);
        ImageView street2 = hexView.findViewById(R.id.street2);
        ImageView street3 = hexView.findViewById(R.id.street3);
        ImageView street4 = hexView.findViewById(R.id.street4);
        ImageView street5 = hexView.findViewById(R.id.icon_dev);

        List<ImageView> streets = new ArrayList<>();
        streets.add(street0);
        streets.add(street1);
        streets.add(street2);
        streets.add(street3);
        streets.add(street4);
        streets.add(street5);

        village0.setVisibility(View.INVISIBLE);
        village1.setVisibility(View.INVISIBLE);
        village2.setVisibility(View.INVISIBLE);
        village3.setVisibility(View.INVISIBLE);
        village4.setVisibility(View.INVISIBLE);
        village5.setVisibility(View.INVISIBLE);

        street0.setVisibility(View.INVISIBLE);
        street1.setVisibility(View.INVISIBLE);
        street2.setVisibility(View.INVISIBLE);
        street3.setVisibility(View.INVISIBLE);
        street4.setVisibility(View.INVISIBLE);
        street5.setVisibility(View.INVISIBLE);

        for (ImageView village : villages) {
            village.setVisibility(View.INVISIBLE);
        }
        for (ImageView street : streets) {
            street.setVisibility(View.INVISIBLE);
        }

        if (tileState.getNumber() == 0) {
            number.setVisibility(View.INVISIBLE);
        } else {
            number.setVisibility(View.VISIBLE);
        }

        for (Building building : tileState.getBuildings()) {
            int buildingResource = getBuildingResource(building);
            if (building.getBuildingType() == BuildingType.VILLAGE || building.getBuildingType() == BuildingType.CITY || building.getBuildingType() == BuildingType.VILLAGE_BUILDING_SPOT) {
                ImageView village = villages.get(building.getIndex());
                village.setImageResource(buildingResource);
                village.setVisibility(View.VISIBLE);
                if (building.getBuildingType() == BuildingType.VILLAGE_BUILDING_SPOT) {
                    village.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ObjectMapper objectMapper = new ObjectMapper();
                            BuildActionValue buildActionValue = new BuildActionValue(building.getIndex(), tileState.getX(), tileState.getY());
                            ActionDTO actionDTO = null;
                            try {
                                actionDTO = new ActionDTO(token, objectMapper.writeValueAsString(buildActionValue), "BUILD_VILLAGE");
                                APIClient.getGameClient().send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else if (buildingCity && building.getBuildingType() == BuildingType.VILLAGE && building.getUsername().equals(username)) {
                    village.setBackgroundResource(R.drawable.circle);
                    village.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ObjectMapper objectMapper = new ObjectMapper();
                            BuildActionValue buildActionValue = new BuildActionValue(building.getIndex(), tileState.getX(), tileState.getY());
                            ActionDTO actionDTO = null;
                            try {
                                actionDTO = new ActionDTO(token, objectMapper.writeValueAsString(buildActionValue), "BUILD_CITY");
                                APIClient.getGameClient().send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else {
                ImageView street = streets.get(building.getIndex());
                street.setImageResource(buildingResource);
                street.setVisibility(View.VISIBLE);
                if (building.getBuildingType() == BuildingType.STREET_BUILDING_SPOT) {
                    street.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ObjectMapper objectMapper = new ObjectMapper();
                            BuildActionValue buildActionValue = new BuildActionValue(building.getIndex(), tileState.getX(), tileState.getY());
                            ActionDTO actionDTO = null;
                            try {
                                actionDTO = new ActionDTO(token, objectMapper.writeValueAsString(buildActionValue), "BUILD_STREET");
                                APIClient.getGameClient().send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        }

        ImageView robberI = hexView.findViewById(R.id.robber);
        if (this.robber[0] == tileState.getX() && this.robber[1] == tileState.getY()) {
            robberI.setVisibility(View.VISIBLE);
            robberI.setImageResource(R.drawable.robber);
        } else if (movingRobber && !tileState.getTileType().isWater()) {
            robberI.setVisibility(View.VISIBLE);
            robberI.setAlpha(125);
            robberI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    MoveRobberData moveRobberData = new MoveRobberData(tileState.getX(), tileState.getY());
                    ActionDTO actionDTO = null;
                    try {
                        actionDTO = new ActionDTO(token, objectMapper.writeValueAsString(moveRobberData), "MOVE_ROBBER");
                        APIClient.getGameClient().send("/app/game/action/" + gameUrl, objectMapper.writeValueAsString(actionDTO)).subscribe();
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            robberI.setVisibility(View.INVISIBLE);
        }

    }

}
