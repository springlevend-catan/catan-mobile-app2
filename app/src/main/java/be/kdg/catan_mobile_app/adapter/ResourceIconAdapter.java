package be.kdg.catan_mobile_app.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.model.TileType;

public class ResourceIconAdapter extends RecyclerView.Adapter{

    List<TileType> resources;

    public ResourceIconAdapter(List<TileType> resources) {
        this.resources = resources;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ImageView view = (ImageView) new ImageView(viewGroup.getContext());
        return new ResourceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        TileType resource= resources.get(i);

        ImageView view = (ImageView) viewHolder.itemView;

        switch (resource){
            case WOOD:
                view.setImageResource(R.drawable.icon_wood);
                break;
            case GRAIN:
                view.setImageResource(R.drawable.icon_grain);
                break;
            case WOOL:
                view.setImageResource(R.drawable.icon_wool);
                break;
            case BRICK:
                view.setImageResource(R.drawable.icon_brick);
                break;
            case ORE:
                view.setImageResource(R.drawable.icon_ore);
                break;
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resources.remove(i);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return resources.size();
    }

    public static class ResourceViewHolder extends RecyclerView.ViewHolder {
        ImageView resourceView;

        public ResourceViewHolder(ImageView resourceView) {
            super(resourceView);
            this.resourceView = resourceView;
        }
    }
}
