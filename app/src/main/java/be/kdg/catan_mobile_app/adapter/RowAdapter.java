package be.kdg.catan_mobile_app.adapter;

import android.content.Context;
import android.service.quicksettings.Tile;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.custom.HexagonView;
import be.kdg.catan_mobile_app.model.TileState;
import be.kdg.catan_mobile_app.model.User;

public class RowAdapter extends ArrayAdapter<List<TileState>> {
    private Context context;
    private ArrayList<List<TileState>> values;
    private String gameUrl;
    private String token;
    private int[] robber;
    private boolean movingRobber;
    private boolean buildingCity;
    private String username;

    public RowAdapter(Context context, ArrayList<List<TileState>> objects, String gameUrl, String token, int[] robber, boolean movingRobber, boolean isBuildingCity, String username) {
        super(context, -1, objects);
        this.context = context;
        this.values = objects;
        this.gameUrl = gameUrl;
        this.token = token;
        this.robber = robber;
        this.movingRobber = movingRobber;
        this.buildingCity = isBuildingCity;
        this.username = username;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        List<TileState> row = values.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.listview_board_row, parent, false);

        RecyclerView rowTiles = rowView.findViewById(R.id.row);
        HexAdapter adapter = new HexAdapter(row, rowTiles.getContext(), gameUrl, token, robber, movingRobber, buildingCity,username);

        LinearLayoutManager layoutManager = new LinearLayoutManager(rowView.getContext(), LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        RecyclerView tiles = rowTiles.findViewById(R.id.row);
        tiles.setHasFixedSize(true);
        tiles.setLayoutManager(layoutManager);
        tiles.setAdapter(adapter);

        return rowView;
    }


}
