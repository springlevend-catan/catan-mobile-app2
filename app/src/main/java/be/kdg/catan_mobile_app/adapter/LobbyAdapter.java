package be.kdg.catan_mobile_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.activities.AllLobiesActivity;
import be.kdg.catan_mobile_app.activities.LobbyActivity;
import be.kdg.catan_mobile_app.activities.MenuActivity;
import be.kdg.catan_mobile_app.model.Lobby;
import be.kdg.catan_mobile_app.model.User;

public class LobbyAdapter extends ArrayAdapter<Lobby> {

    List<Lobby> values;
    Context context;
    String username;

    public LobbyAdapter(Context context,List<Lobby> objects, String username) {
        super(context, -1, objects);
        this.values = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Lobby lobby = values.get(position);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View lobbyView = inflater.inflate(R.layout.listview_lobby_row, parent, false);

        TextView tv_lobbyUrl = lobbyView.findViewById(R.id.lobbyUrl);
        tv_lobbyUrl.setText(lobby.getUrl());
        TextView players = lobbyView.findViewById(R.id.lobby_players);
        players.setText(String.valueOf(lobby.getAllUsers().length));

        Button joinLobby = lobbyView.findViewById(R.id.joinLobby);
        joinLobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(lobbyView.getContext(), LobbyActivity.class);
                intent.putExtra("USERNAME", username);
                intent.putExtra("URL", lobby.getUrl());
                lobbyView.getContext().startActivity(intent);
            }
        });

        return lobbyView;
    }
}
