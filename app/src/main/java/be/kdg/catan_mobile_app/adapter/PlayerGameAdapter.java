package be.kdg.catan_mobile_app.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.model.Player;

public class PlayerGameAdapter extends RecyclerView.Adapter<PlayerGameAdapter.PlayerViewHolder> {

    private List<Player> values;
    private int selected;
    private String playerWithTurn;

    public PlayerGameAdapter(List<Player> objects, int selected, String playerWithTurn) {
        this.values = objects;
        this.selected = selected;
        this.playerWithTurn = playerWithTurn;
    }

    public View getView(int position, View playerView) {
        Player player = values.get(position);
        ImageView bg_paper = playerView.findViewById(R.id.bg_paper);
        ImageView iv_avatar = playerView.findViewById(R.id.iv_avatar);
        ImageView icon_res = playerView.findViewById(R.id.icon_res);
        ImageView icon_dev = playerView.findViewById(R.id.icon_dev);
        ImageView iv_points = playerView.findViewById(R.id.iv_points);
        ImageView iv_knights = playerView.findViewById(R.id.iv_knight);
        ImageView iv_path = playerView.findViewById(R.id.iv_path);
        TextView username = playerView.findViewById(R.id.username);
        TextView tv_res = playerView.findViewById(R.id.tv_res);
        TextView tv_dev = playerView.findViewById(R.id.tv_dev);
        TextView tv_path = playerView.findViewById(R.id.tv_path);
        TextView tv_knight = playerView.findViewById(R.id.tv_knight);
        TextView tv_points = playerView.findViewById(R.id.tv_points);

        ImageView playerWithTurn = playerView.findViewById(R.id.playerWithTurn);

        if (player.getUsername().equals(this.playerWithTurn)) {
            playerWithTurn.setVisibility(View.VISIBLE);
        } else {
            playerWithTurn.setVisibility(View.INVISIBLE);
        }

        if (player.getTurnOrder() == selected) {
            bg_paper.setVisibility(View.VISIBLE);
            icon_res.setVisibility(View.VISIBLE);
            icon_dev.setVisibility(View.VISIBLE);
            username.setVisibility(View.VISIBLE);
            tv_res.setVisibility(View.VISIBLE);
            tv_dev.setVisibility(View.VISIBLE);
            iv_points.setVisibility(View.VISIBLE);
            iv_knights.setVisibility(View.VISIBLE);
            iv_path.setVisibility(View.VISIBLE);
            username.setText(player.getUsername());
            tv_path.setVisibility(View.VISIBLE);
            tv_knight.setVisibility(View.VISIBLE);
            tv_points.setVisibility(View.VISIBLE);

            final int[] sum = new int[]{0};
            player.getResources().entrySet().forEach(e -> sum[0] = sum[0] + e.getValue());
            tv_res.setText(String.valueOf(sum[0]));
            tv_dev.setText(String.valueOf(0));

            tv_knight.setText(String.valueOf(player.getNumberOfKnights()));
            tv_path.setText(String.valueOf(player.getLongestRoad()));
            tv_points.setText(String.valueOf(player.getPoints()));

        } else {
            bg_paper.setVisibility(View.GONE);
            icon_res.setVisibility(View.GONE);
            icon_dev.setVisibility(View.GONE);
            username.setVisibility(View.GONE);
            tv_res.setVisibility(View.GONE);
            tv_dev.setVisibility(View.GONE);
            iv_points.setVisibility(View.GONE);
            iv_path.setVisibility(View.GONE);
            iv_knights.setVisibility(View.GONE);
            tv_path.setVisibility(View.GONE);
            tv_knight.setVisibility(View.GONE);
            tv_points.setVisibility(View.GONE);
        }

        iv_avatar.setImageResource(R.drawable.avatar2);

        iv_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = player.getTurnOrder();
                notifyDataSetChanged();
                System.out.println(player.getTurnOrder());
                playerView.getParent().childDrawableStateChanged(playerView);
            }
        });
        return playerView;
    }


    @NonNull
    @Override
    public PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = (View) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listview_players_game_row, viewGroup, false);
        return new PlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerViewHolder playerViewHolder, int i) {
        playerViewHolder.playerView = getView(i, playerViewHolder.playerView);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public static class PlayerViewHolder extends RecyclerView.ViewHolder {
        View playerView;

        public PlayerViewHolder(View playerView) {
            super(playerView);
            this.playerView = playerView;
        }
    }
}
