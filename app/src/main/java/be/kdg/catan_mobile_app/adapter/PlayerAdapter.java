package be.kdg.catan_mobile_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import be.kdg.catan_mobile_app.R;
import be.kdg.catan_mobile_app.model.User;

public class PlayerAdapter extends ArrayAdapter<User> {
    private final Context context;
    private final User[] values;

    public PlayerAdapter(Context context, User[] values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User user = values[position];
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.listview_players_row, parent, false);
        TextView tv_username = (TextView) rowView.findViewById(R.id.tv_username);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.iv_profile);
        tv_username.setText(user.getUsername());

        switch (user.getProfilePicture()){
            case 0:
                imageView.setImageResource(R.drawable.avatar0);
                break;
            case 1:
                imageView.setImageResource(R.drawable.avatar1);
                break;
            case 2:
                imageView.setImageResource(R.drawable.avatar2);
                break;
            case 3:
                imageView.setImageResource(R.drawable.avatar3);
                break;
            case 4:
                imageView.setImageResource(R.drawable.avatar4);
                break;
            case 5:
                imageView.setImageResource(R.drawable.avatar5);
                break;
            case 6:
                imageView.setImageResource(R.drawable.avatar6);
                break;
            case 7:
                imageView.setImageResource(R.drawable.avatar7);
                break;
            case 8:
                imageView.setImageResource(R.drawable.avatar8);
                break;
            case 9:
                imageView.setImageResource(R.drawable.avatar9);
                break;
        }

        Button btn_kick = rowView.findViewById(R.id.btn_kick);
        if (user.isHost()){
            btn_kick.setVisibility(View.INVISIBLE);
        }

        return rowView;
    }
}
