package be.kdg.catan_mobile_app.rest;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;


//replace with websockets
public abstract class RestTask implements Callback {
    static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    protected Gson jsonConverter;
    protected String bearer;

    protected final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build();

    protected Call call;

    public RestTask(String bearer){
        this.bearer = bearer;
        this.jsonConverter = new GsonBuilder().create();
    }

    public RestTask() {
        this ("No bearer");
    }

    protected abstract void execute();
}
