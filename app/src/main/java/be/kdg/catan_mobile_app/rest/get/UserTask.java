package be.kdg.catan_mobile_app.rest.get;

import java.io.IOException;

import be.kdg.catan_mobile_app.model.User;
import be.kdg.catan_mobile_app.rest.RestTask;
import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

//replace with websockets
public class UserTask extends RestTask {
    private Listener listener;
    private String userId;

    public UserTask(String bearer, Listener listener, String userId) {
        super(bearer);
        this.listener = listener;
        this.userId = userId;
    }


    @Override
    public void execute() {
        Request request = new Request.Builder()
                .addHeader("User", bearer)
                .get()
                .url("" + userId) //fill in url
                .build();
        call = okHttpClient.newCall(request);
        call.enqueue(this);
    }


    @Override
    public void onFailure(Call call, IOException e) {
        listener.onFailedRetrieve(e.getMessage());
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        User user = jsonConverter.fromJson(response.body().charStream(), User.class);
        listener.onSuccessfulRetrieve(user);
    }


    public interface Listener {
        void onSuccessfulRetrieve(User user);
        void onFailedRetrieve(String message);
    }
}
