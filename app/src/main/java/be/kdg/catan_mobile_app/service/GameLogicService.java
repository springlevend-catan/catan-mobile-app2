package be.kdg.catan_mobile_app.service;

import java.util.List;

import be.kdg.catan_mobile_app.model.Lobby;
import be.kdg.catan_mobile_app.model.TileState;
import be.kdg.catan_mobile_app.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GameLogicService {

    @Headers("Content-Type: application/json")
    @POST("/api/lobby/create")
    Call<String> createLobby(@Body String body);

    @Headers("Content-Type: application/json")
    @GET("/api/game/villageSpots/{url}")
    Call<List<List<TileState>>> getVillageSpots(@Path("url") String url);

    @Headers("Content-Type: application/json")
    @GET("/api/game/streetSpots/{url}")
    Call<List<List<TileState>>> getStreetSpots(@Path("url") String url);

    @Headers("Content-Type: application/json")
    @GET("/api/lobby/available")
    Call<List<Lobby>> getAvailableLobbies();
}
