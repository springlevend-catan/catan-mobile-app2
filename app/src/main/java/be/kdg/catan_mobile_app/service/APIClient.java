package be.kdg.catan_mobile_app.service;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class APIClient {

    private static final String AUTHENTICATION_URL = "http://192.168.1.5:5000";
    private static final String GAME_LOGIC_URL = "http://192.168.1.5:5500";
    private static final String TAG = "GAMESOCKET";
    private static Retrofit authenticationRetrofit = null;
    private static Retrofit gamelogicRetrofit = null;

    private static StompClient gameClient = null;

    public static Retrofit getAuthenticationClient() {
        if (authenticationRetrofit == null) {
            authenticationRetrofit = new Retrofit.Builder()
                    .baseUrl(AUTHENTICATION_URL)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                    .client(new OkHttpClient.Builder()
                            .readTimeout(60, TimeUnit.SECONDS)
                            .connectTimeout(60,TimeUnit.SECONDS).build())
                    .build();
        }
        return authenticationRetrofit;
    }

    public static Retrofit getGameLogicClient(String token) {
        if (gamelogicRetrofit == null) {
            gamelogicRetrofit = new Retrofit.Builder()
                    .baseUrl(GAME_LOGIC_URL)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                    .client(new OkHttpClient.Builder().addInterceptor(chain -> {
                        Request original = chain.request();

                        Request request = original.newBuilder()
                                .header("Authorization", token)
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    })
                            .readTimeout(60, TimeUnit.SECONDS)
                            .connectTimeout(60,TimeUnit.SECONDS).build())
                    .build();
        }
        return gamelogicRetrofit;
    }

    public static StompClient getGameClient(){
        if (gameClient==null){
            gameClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "http://192.168.1.5:5500/ws/websocket");
            gameClient.lifecycle().subscribe(lifecycleEvent -> {
                switch (lifecycleEvent.getType()) {

                    case OPENED:
                        Log.d(TAG, "Stomp connection opened");
                        break;

                    case ERROR:
                        Log.e(TAG, "Error", lifecycleEvent.getException());
                        break;

                    case CLOSED:
                        Log.d(TAG, "Stomp connection closed");
                        break;
                }
            });
        }
        gameClient.connect();
        return gameClient;
    }
}