package be.kdg.catan_mobile_app.service;

import be.kdg.catan_mobile_app.model.User;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Part;
public interface AuthenticationService {
    @Headers("Content-Type: application/json")
    @POST("users/sign-in")
    Call<String> signIn(@Body User user);

    @Headers("Content-Type: application/json")
    @POST("users/sign-up")
    Call<User> signUp(@Body User user);
}
